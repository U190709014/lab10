package stack;

public class StackImpl implements Stack {

    StackItem top =null;



    public void push(Object item){
        StackItem Box = new StackItem(item);
        StackItem PreviousTop=top;
        top=Box;
        Box.setNext(PreviousTop);


    }
    public Object pop(){
        StackItem OldTop=top;
        top=top.getNext();
        return OldTop.getItem();

    }
    public boolean empty(){
        return top==null;
    }

}
